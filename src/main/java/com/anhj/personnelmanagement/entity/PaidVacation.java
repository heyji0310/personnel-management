package com.anhj.personnelmanagement.entity;

import com.anhj.personnelmanagement.enums.PaidVacationType;
import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import com.anhj.personnelmanagement.model.paidVacationModel.AnnualOfUsedDataUpdateRequest;
import com.anhj.personnelmanagement.model.paidVacationModel.PaidVacationRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PaidVacation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paidVacationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hiSystemsEmployeeId", nullable = false)
    private HiSystemsEmployee hiSystemsEmployee;

    @Column(nullable = false)
    private PaidVacationType paidVacationType;

    @Column(nullable = false)
    private Double occurrenceOfAnnual;

    @Column
    private Double annualOfUsed;

    @Column
    private LocalDate dateUseStart;

    @Column
    private LocalDate dateUseEnd;

    private LocalDateTime dateCrete;
    private LocalDateTime dateUpdate;

    public void putAnnualOfUsedData(AnnualOfUsedDataUpdateRequest request) {
        this.annualOfUsed = request.getAnnualOfUsed();
        this.dateUseStart = request.getDateUseStart();
        this.dateUseEnd = request.getDateUseEnd();
        this.dateUpdate = LocalDateTime.now();
    }

    private PaidVacation(PaidVacationBuilder builder) {
        this.hiSystemsEmployee = builder.hiSystemsEmployee;
        this.paidVacationType = builder.paidVacationType;
        this.occurrenceOfAnnual = builder.occurrenceOfAnnual;
        this.dateCrete = builder.dateCrete;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class PaidVacationBuilder implements CommonModelBuilder<PaidVacation> {
        private final HiSystemsEmployee hiSystemsEmployee;
        private final PaidVacationType paidVacationType;
        private final Double occurrenceOfAnnual;
        private final LocalDateTime dateCrete;
        private final LocalDateTime dateUpdate;

        public PaidVacationBuilder(HiSystemsEmployee hiSystemsEmployee, PaidVacationRequest request) {
            this.hiSystemsEmployee = hiSystemsEmployee;
            this.paidVacationType = request.getPaidVacationType();
            this.occurrenceOfAnnual = request.getOccurrenceOfAnnual();
            this.dateCrete = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public PaidVacation build() {
            return new PaidVacation(this);
        }
    }
}
