package com.anhj.personnelmanagement.entity;

import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeUpdateRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeWithdrawalUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HiSystemsEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;

    @Column(nullable = false, length = 30, unique = true)
    private String loginId;

    @Column(nullable = false, length = 30)
    private String loginPw;

    @Column(nullable = false, unique = true)
    private Integer employeeNumber;

    @Column(nullable = false, length = 20)
    private String employeeName;

    @Column(nullable = false)
    private DepartmentType departmentType;

    @Column(nullable = false)
    private PositionType positionType;

    @Column(nullable = false, length = 254)
    private String idPictureUrl;

    @Column(nullable = false, length = 15)
    private String phone;

    @Column(nullable = false, length = 30)
    private String email;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false, length = 10)
    private String authority;

    @Column(nullable = false, length = 50)
    private String homeAddress;

    @Column(nullable = false, length = 15)
    private String theOfficeTel;

    @Column(nullable = false, length = 15)
    private String theOfficeFax;

    @Column(nullable = false, length = 50)
    private String theOfficeAddress;

    @Column(nullable = false)
    private LocalDate dateJoining;

    private LocalDate dateWithdrawal;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDateWithdrawal(HiSystemsEmployeeWithdrawalUpdateRequest request) {
        this.isEnable = false;
        this.dateWithdrawal = request.getDateWithdrawal();
    }

    public void putHiSystemEmployeeLoginData(HiSystemsEmployeeUpdateRequest updateRequest) {
        this.loginPw = updateRequest.getLoginPw();
        this.employeeName = updateRequest.getEmployeeName();
        this.departmentType = updateRequest.getDepartmentType();
        this.positionType = updateRequest.getPositionType();
        this.idPictureUrl = updateRequest.getIdPictureUrl();
        this.phone = updateRequest.getPhone();
        this.email = updateRequest.getEmail();
        this.birthday = updateRequest.getBirthday();
        this.homeAddress = updateRequest.getHomeAddress();
        this.theOfficeTel = updateRequest.getTheOfficeTel();
        this.theOfficeFax = updateRequest.getTheOfficeFax();
        this.theOfficeAddress = updateRequest.getTheOfficeAddress();
        this.dateUpdate = LocalDateTime.now();
    }

    private HiSystemsEmployee(HiSystemsEmployeeBuilder builder) {
        this.loginId = builder.loginId;
        this.loginPw = builder.loginPw;
        this.employeeNumber = builder.employeeNumber;
        this.employeeName = builder.employeeName;
        this.departmentType = builder.departmentType;
        this.positionType = builder.positionType;
        this.idPictureUrl = builder.idPictureUrl;
        this.phone = builder.phone;
        this.email = builder.email;
        this.birthday = builder.birthday;
        this.isEnable = builder.isEnable;
        this.authority = builder.authority;
        this.homeAddress = builder.homeAddress;
        this.theOfficeTel = builder.theOfficeTel;
        this.theOfficeFax = builder.theOfficeFax;
        this.theOfficeAddress = builder.theOfficeAddress;
        this.dateJoining = builder.dateJoining;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class HiSystemsEmployeeBuilder implements CommonModelBuilder<HiSystemsEmployee> {
        private final String loginId;
        private final String loginPw;
        private final Integer employeeNumber;
        private final String employeeName;
        private final DepartmentType departmentType;
        private final PositionType positionType;
        private final String idPictureUrl;
        private final String phone;
        private final String email;
        private final LocalDate birthday;
        private final Boolean isEnable;
        private final String authority;
        private final String homeAddress;
        private final String theOfficeTel;
        private final String theOfficeFax;
        private final String theOfficeAddress;
        private final LocalDate dateJoining;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public HiSystemsEmployeeBuilder(HiSystemsEmployeeRequest hiSystemsEmployeeRequest) {
            this.loginId = hiSystemsEmployeeRequest.getLoginId();
            this.loginPw = hiSystemsEmployeeRequest.getLoginPw();
            this.employeeNumber = hiSystemsEmployeeRequest.getEmployeeNumber();
            this.employeeName = hiSystemsEmployeeRequest.getEmployeeName();
            this.departmentType = hiSystemsEmployeeRequest.getDepartmentType();
            this.positionType = hiSystemsEmployeeRequest.getPositionType();
            this.idPictureUrl = hiSystemsEmployeeRequest.getIdPictureUrl();
            this.phone = hiSystemsEmployeeRequest.getPhone();
            this.email = hiSystemsEmployeeRequest.getEmail();
            this.birthday = hiSystemsEmployeeRequest.getBirthday();
            this.isEnable = true;
            this.authority = hiSystemsEmployeeRequest.getAuthority();
            this.homeAddress = hiSystemsEmployeeRequest.getHomeAddress();
            this.theOfficeTel = hiSystemsEmployeeRequest.getTheOfficeTel();
            this.theOfficeFax = hiSystemsEmployeeRequest.getTheOfficeFax();
            this.theOfficeAddress = hiSystemsEmployeeRequest.getTheOfficeAddress();
            this.dateJoining = hiSystemsEmployeeRequest.getDateJoining();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public HiSystemsEmployee build() {
            return new HiSystemsEmployee(this);
        }
    }
}
