package com.anhj.personnelmanagement.entity;

import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TimeAttendanceRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long timeAttendanceRecordId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hiSystemEmployeeId", nullable = false)
    private HiSystemsEmployee hiSystemsEmployee;

    @Column
    private LocalDateTime goToTheOffice;

    @Column
    private LocalDateTime leaveTheOffice;

    @Column
    private LocalDateTime outOfOffice;

    @Column
    private LocalDateTime backToOffice;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putLeaveTheOffice() {
        this.leaveTheOffice = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putOutOfOffice() {
        this.outOfOffice = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putBackToOffice() {
        this.backToOffice = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();
    }

    private TimeAttendanceRecord(TimeAttendanceRecordBuilder builder) {
        this.hiSystemsEmployee = builder.hiSystemsEmployee;
        this.goToTheOffice = builder.goToTheOffice;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class TimeAttendanceRecordBuilder implements CommonModelBuilder<TimeAttendanceRecord> {
        private final HiSystemsEmployee hiSystemsEmployee;
        private final LocalDateTime goToTheOffice;
        private final LocalDateTime dateUpdate;

        public TimeAttendanceRecordBuilder(HiSystemsEmployee hiSystemsEmployee) {
            this.hiSystemsEmployee = hiSystemsEmployee;
            this.goToTheOffice = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public TimeAttendanceRecord build() {
            return new TimeAttendanceRecord(this);
        }
    }
}
