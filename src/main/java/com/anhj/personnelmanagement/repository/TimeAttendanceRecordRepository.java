package com.anhj.personnelmanagement.repository;

import com.anhj.personnelmanagement.entity.TimeAttendanceRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeAttendanceRecordRepository extends JpaRepository<TimeAttendanceRecord, Long> {
    TimeAttendanceRecord findAllByHiSystemsEmployee_EmployeeIdOrderByHiSystemsEmployeeDateUpdateDesc(long employeeId);
}
