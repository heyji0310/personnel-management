package com.anhj.personnelmanagement.repository;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HiSystemEmployeeRepository extends JpaRepository<HiSystemsEmployee, Long> {
    List<HiSystemsEmployee> findAllByDepartmentTypeOrderByDepartmentTypeAsc(DepartmentType departmentType);

    List<HiSystemsEmployee> findAllByPositionTypeOrderByPositionTypeAsc(PositionType positionType);

    HiSystemsEmployee findAllByLoginIdOrderByEmployeeNumberAsc(String loginId);
}
