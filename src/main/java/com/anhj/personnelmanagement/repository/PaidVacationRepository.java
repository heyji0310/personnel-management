package com.anhj.personnelmanagement.repository;

import com.anhj.personnelmanagement.entity.PaidVacation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaidVacationRepository extends JpaRepository<PaidVacation, Long> {
}
