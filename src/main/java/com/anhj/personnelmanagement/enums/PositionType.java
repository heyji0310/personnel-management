package com.anhj.personnelmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PositionType {
    STAFF ("사원")
    , ASSISTANT_MANAGER("대리")
    , SUPERVISOR("과장")
    , CHIEF_MANAGER("차장")
    , DEPARTMENT_HEAD("부장")
    ;

    private final String positName;
}
