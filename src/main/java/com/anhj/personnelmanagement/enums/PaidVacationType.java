package com.anhj.personnelmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PaidVacationType {
    ANNUAL("연차")
    , HALF_LEAVE("반차")
    ;

    private final String paidVacationTypeName;
}
