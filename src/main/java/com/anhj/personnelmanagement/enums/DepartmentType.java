package com.anhj.personnelmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DepartmentType {
    GENERAL_AFFAIRS_DEPART("총무부")
    , MANAGEMENT_DEPART("관리부")
    , SALES_DEPART("영업부")
    ;

    private final String departmentTypeName;
}
