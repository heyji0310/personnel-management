package com.anhj.personnelmanagement.model.hiSystemsEployeeModel;

import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class HiSystemsEmployeeRequest {
    @ApiModelProperty(notes = "로그인 아이디")
    @NotNull
    @Length(min = 3, max = 30)
    private String loginId;

    @ApiModelProperty(notes = "로그인 패스워드")
    @NotNull
    @Length(min = 5, max = 30)
    private String loginPw;

    @ApiModelProperty(notes = "사원번호", example = "0")
    @NotNull
    private Integer employeeNumber;

    @ApiModelProperty(notes = "회원이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "부서")
    @NotNull
    private DepartmentType departmentType;

    @ApiModelProperty(notes = "직급")
    @NotNull
    private PositionType positionType;

    @ApiModelProperty(notes = "사원 사진")
    @NotNull
    @Length(max = 254)
    private String idPictureUrl;

    @ApiModelProperty(notes = "연락처")
    @NotNull
    @Length(min = 12, max = 15)
    private String phone;

    @ApiModelProperty(notes = "이메일 주소")
    @NotNull
    @Length(max = 30)
    private String email;

    @ApiModelProperty(notes = "생년월일")
    @NotNull
    private LocalDate birthday;

    @ApiModelProperty(notes = "관리자코드")
    @NotNull
    @Length(max = 10)
    private String authority;

    @ApiModelProperty(notes = "집 주소")
    @NotNull
    @Length(max = 50)
    private String homeAddress;

    @ApiModelProperty(notes = "사무실내선전화")
    @NotNull
    @Length(max = 15)
    private String theOfficeTel;

    @ApiModelProperty(notes = "입사일자")
    @NotNull
    private LocalDate dateJoining;

    @ApiModelProperty(notes = "사무실팩스")
    @NotNull
    @Length(max = 15)
    private String theOfficeFax;

    @ApiModelProperty(notes = "사무실 주소")
    @NotNull
    @Length(max = 50)
    private String theOfficeAddress;
}
