package com.anhj.personnelmanagement.model.hiSystemsEployeeModel;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HiSystemEmployeeItem {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long employeeId;

    @ApiModelProperty(notes = "로그인 아이디")
    private String loginId;

    @ApiModelProperty(notes = "로그인 패스워드")
    private String loginPw;

    @ApiModelProperty(notes = "사원번호", example = "0")
    private Integer employeeNumber;

    @ApiModelProperty(notes = "회원이름")
    private String employeeName;

    @ApiModelProperty(notes = "부서")
    private String departmentType;

    @ApiModelProperty(notes = "직급")
    private String positionType;

    @ApiModelProperty(notes = "사원 사진")
    private String idPictureUrl;

    @ApiModelProperty(notes = "연락처")
    private String phone;

    @ApiModelProperty(notes = "이메일 주소")
    private String email;

    @ApiModelProperty(notes = "생년월일")
    private LocalDate birthday;

    @ApiModelProperty(notes = "유효회원인가")
    private String isEnable;

    @ApiModelProperty(notes = "관리자코드")
    private String authority;

    @ApiModelProperty(notes = "집 주소")
    private String homeAddress;

    @ApiModelProperty(notes = "사무실내선전화")
    private String theOfficeTel;

    @ApiModelProperty(notes = "사무실팩스")
    private String theOfficeFax;

    @ApiModelProperty(notes = "주소")
    private String theOfficeAddress;

    private HiSystemEmployeeItem(HiSystemEmployeeItemBuilder itemBuilder) {
        this.employeeId = itemBuilder.employeeId;
        this.loginId = itemBuilder.loginId;
        this.loginPw = itemBuilder.loginPw;
        this.employeeNumber = itemBuilder.employeeNumber;
        this.employeeName = itemBuilder.employeeName;
        this.departmentType = itemBuilder.departmentType;
        this.positionType = itemBuilder.positionType;
        this.idPictureUrl = itemBuilder.idPictureUrl;
        this.phone = itemBuilder.phone;
        this.email = itemBuilder.email;
        this.birthday = itemBuilder.birthday;
        this.isEnable = itemBuilder.isEnable;
        this.authority = itemBuilder.authority;
        this.homeAddress = itemBuilder.homeAddress;
        this.theOfficeTel = itemBuilder.theOfficeTel;
        this.theOfficeFax = itemBuilder.theOfficeFax;
        this.theOfficeAddress = itemBuilder.theOfficeAddress;
    }

    public static class HiSystemEmployeeItemBuilder implements CommonModelBuilder<HiSystemEmployeeItem> {
        private final Long employeeId;
        private final String loginId;
        private final String loginPw;
        private final Integer employeeNumber;
        private final String employeeName;
        private final String departmentType;
        private final String positionType;
        private final String idPictureUrl;
        private final String phone;
        private final String email;
        private final LocalDate birthday;
        private final String isEnable;
        private final String authority;

        private final String homeAddress;
        private final String theOfficeTel;
        private final String theOfficeFax;
        private final String theOfficeAddress;

        public HiSystemEmployeeItemBuilder(HiSystemsEmployee hiSystemsEmployee) {
            this.employeeId = hiSystemsEmployee.getEmployeeId();
            this.loginId = hiSystemsEmployee.getLoginId();
            this.loginPw = hiSystemsEmployee.getLoginPw();
            this.employeeNumber = hiSystemsEmployee.getEmployeeNumber();
            this.employeeName = hiSystemsEmployee.getEmployeeName();
            this.departmentType = hiSystemsEmployee.getDepartmentType().getDepartmentTypeName();
            this.positionType = hiSystemsEmployee.getPositionType().getPositName();
            this.idPictureUrl = hiSystemsEmployee.getIdPictureUrl();
            this.phone = hiSystemsEmployee.getPhone();
            this.email = hiSystemsEmployee.getEmail();
            this.birthday = hiSystemsEmployee.getBirthday();
            this.isEnable = hiSystemsEmployee.getIsEnable() ? "접근가능" : "접근불가능";  // true면 재직상태, flase면 그 이외(퇴직, 휴직 ect.)
            this.authority = hiSystemsEmployee.getAuthority();
            this.homeAddress = hiSystemsEmployee.getHomeAddress();
            this.theOfficeTel = hiSystemsEmployee.getTheOfficeTel();
            this.theOfficeFax = hiSystemsEmployee.getTheOfficeFax();
            this.theOfficeAddress = hiSystemsEmployee.getTheOfficeAddress();
        }

        @Override
        public HiSystemEmployeeItem build() {
            return new HiSystemEmployeeItem(this);
        }
    }
}
