package com.anhj.personnelmanagement.model.hiSystemsEployeeModel;

import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class HiSystemsEmployeeUpdateRequest {
    @ApiModelProperty(notes = "로그인 패스워드")
    @Length(min = 5, max = 30)
    private String loginPw;

    @ApiModelProperty(notes = "회원이름")
    @Length(min = 2, max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "부서")
    private DepartmentType departmentType;

    @ApiModelProperty(notes = "직급")
    private PositionType positionType;

    @ApiModelProperty(notes = "사원 사진")
    @Length(max = 254)
    private String idPictureUrl;

    @ApiModelProperty(notes = "연락처")
    @Length(min = 12, max = 15)
    private String phone;

    @ApiModelProperty(notes = "이메일 주소")
    @Length(max = 30)
    private String email;

    @ApiModelProperty(notes = "생년월일")
    private LocalDate birthday;

    @ApiModelProperty(notes = "집 주소")
    @Length(max = 50)
    private String homeAddress;

    @ApiModelProperty(notes = "사무실내선전화")
    @Length(max = 15)
    private String theOfficeTel;

    @ApiModelProperty(notes = "사무실팩스")
    @Length(max = 15)
    private String theOfficeFax;

    @ApiModelProperty(notes = "사무실주소")
    @Length(max = 50)
    private String theOfficeAddress;
}
