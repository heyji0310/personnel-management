package com.anhj.personnelmanagement.model.hiSystemsEployeeModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HiSystemsEmployeeWithdrawalUpdateRequest {
    @ApiModelProperty(notes = "퇴사일자")
    private LocalDate dateWithdrawal;
}
