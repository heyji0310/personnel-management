package com.anhj.personnelmanagement.model.paidVacationModel;

import com.anhj.personnelmanagement.entity.PaidVacation;
import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PaidVacationItem {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long hiSystemsEmployeeId;

    private String employeeInfo;

    @ApiModelProperty(notes = "발생연차갯수")
    private String occurrenceOfAnnual;

    @ApiModelProperty(notes = "사용연차갯수")
    private String annualOfUsed;

    private PaidVacationItem(PaidVacationItemBuilder builder) {
        this.hiSystemsEmployeeId = builder.hiSystemsEmployeeId;
        this.employeeInfo = builder.employeeInfo;
        this.occurrenceOfAnnual = builder.occurrenceOfAnnual;
        this.annualOfUsed = builder.annualOfUsed;
    }

    public static class PaidVacationItemBuilder implements CommonModelBuilder<PaidVacationItem> {
        private final Long hiSystemsEmployeeId;
        private final String employeeInfo;
        private final String occurrenceOfAnnual;
        private final String annualOfUsed;

        public PaidVacationItemBuilder(PaidVacation paidVacation) {
            this.hiSystemsEmployeeId = paidVacation.getPaidVacationId();

            this.employeeInfo =
                    "[" + paidVacation.getHiSystemsEmployee().getDepartmentType().getDepartmentTypeName() + "] "
                    + paidVacation.getHiSystemsEmployee().getEmployeeName() + " "
                    + paidVacation.getHiSystemsEmployee().getPositionType().getPositName()
                    + ", 사원번호: " + paidVacation.getHiSystemsEmployee().getEmployeeNumber();

            this.occurrenceOfAnnual = paidVacation.getOccurrenceOfAnnual() + "일";
            this.annualOfUsed = paidVacation.getAnnualOfUsed() + "일";
        }

        @Override
        public PaidVacationItem build() {
            return new PaidVacationItem(this);
        }
    }
}
