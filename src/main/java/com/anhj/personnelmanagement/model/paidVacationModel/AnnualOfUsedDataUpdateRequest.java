package com.anhj.personnelmanagement.model.paidVacationModel;

import com.anhj.personnelmanagement.enums.PaidVacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AnnualOfUsedDataUpdateRequest {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long hiSystemsEmployeeId;

    @ApiModelProperty(notes = "유급휴가 타입")
    private PaidVacationType paidVacationType;

    @ApiModelProperty(notes = "사용연차갯수")
    private Double annualOfUsed;

    @ApiModelProperty(notes = "사용 시작일")
    private LocalDate dateUseStart;

    @ApiModelProperty(notes = "사용 종료일")
    private LocalDate dateUseEnd;
}
