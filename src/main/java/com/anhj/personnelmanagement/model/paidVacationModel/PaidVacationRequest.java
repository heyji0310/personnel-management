package com.anhj.personnelmanagement.model.paidVacationModel;

import com.anhj.personnelmanagement.enums.PaidVacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PaidVacationRequest {
    @ApiModelProperty(notes = "사원 시퀀스", required = true)
    @NotNull
    private Long hiSystemsEmployeeId;

    @ApiModelProperty(notes = "유급휴가 종류", required = true)
    @NotNull
    private PaidVacationType paidVacationType;

    @ApiModelProperty(notes = "발생연차갯수", required = true)
    private Double occurrenceOfAnnual;
}
