package com.anhj.personnelmanagement.model.timeAttendanceRecordModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TimeAttendanceRecordRequest {
    @ApiModelProperty(notes = "사원 시퀀스")
    @NotNull
    private Long hiSystemsEmployeeId;
}
