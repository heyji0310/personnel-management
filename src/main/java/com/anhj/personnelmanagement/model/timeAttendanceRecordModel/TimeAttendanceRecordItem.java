package com.anhj.personnelmanagement.model.timeAttendanceRecordModel;

import com.anhj.personnelmanagement.entity.TimeAttendanceRecord;
import com.anhj.personnelmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TimeAttendanceRecordItem {
    private Long timeAttendanceRecordId;
    private String employeeInfo;
    private String goToTheOffice;
    private String leaveTheOffice;
    private String outOfOffice;
    private String backToOffice;

    private TimeAttendanceRecordItem(TimeAttendanceRecordItemBuilder builder) {
        this.timeAttendanceRecordId = builder.timeAttendanceRecordId;
        this.employeeInfo = builder.employeeInfo;
        this.goToTheOffice = builder.goToTheOffice;
        this.leaveTheOffice = builder.leaveTheOffice;
        this.outOfOffice = builder.outOfOffice;
        this.backToOffice = builder.backToOffice;
    }

    public static class TimeAttendanceRecordItemBuilder implements CommonModelBuilder<TimeAttendanceRecordItem> {
        private final Long timeAttendanceRecordId;
        private final String employeeInfo;
        private final String goToTheOffice;
        private final String leaveTheOffice;
        private final String outOfOffice;
        private final String backToOffice;

        public TimeAttendanceRecordItemBuilder(TimeAttendanceRecord timeAttendanceRecord) {
            this.timeAttendanceRecordId = timeAttendanceRecord.getTimeAttendanceRecordId();

            this.employeeInfo =
                    "[" + timeAttendanceRecord.getHiSystemsEmployee().getDepartmentType().getDepartmentTypeName() + "] "
                    + timeAttendanceRecord.getHiSystemsEmployee().getEmployeeName() + " "
                    + timeAttendanceRecord.getHiSystemsEmployee().getPositionType().getPositName()
                    + "사원번호: " + timeAttendanceRecord.getHiSystemsEmployee().getEmployeeNumber();

            this.goToTheOffice = timeAttendanceRecord.getGoToTheOffice().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
            this.leaveTheOffice = timeAttendanceRecord.getLeaveTheOffice().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
            this.outOfOffice = timeAttendanceRecord.getOutOfOffice().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
            this.backToOffice = timeAttendanceRecord.getBackToOffice().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
        }

        @Override
        public TimeAttendanceRecordItem build() {
            return new TimeAttendanceRecordItem(this);
        }
    }
}
