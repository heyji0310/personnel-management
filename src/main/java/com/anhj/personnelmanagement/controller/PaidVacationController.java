package com.anhj.personnelmanagement.controller;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.model.CommonResult;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.paidVacationModel.AnnualOfUsedDataUpdateRequest;
import com.anhj.personnelmanagement.model.paidVacationModel.PaidVacationItem;
import com.anhj.personnelmanagement.model.paidVacationModel.PaidVacationRequest;
import com.anhj.personnelmanagement.service.HiSystemEmployeeService;
import com.anhj.personnelmanagement.service.PaidVacationService;
import com.anhj.personnelmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "연차 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/paid-vacation")
public class PaidVacationController {
    private final PaidVacationService paidVacationService;
    private final HiSystemEmployeeService hiSystemEmployeeService;

    @ApiOperation(value = "연차 발생 내역 등록")
    @PostMapping("/new")
    public CommonResult setPaidVacation(@RequestBody @Valid PaidVacationRequest request) {
        HiSystemsEmployee hiSystemsEmployee = hiSystemEmployeeService.getEmployeeData(request.getHiSystemsEmployeeId());
        paidVacationService.setPaidVacation(hiSystemsEmployee, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연차 사용 내역 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "hiSystemsEmployeeId", value = "사원 시퀀스", required = true)
    )
    @PutMapping("/annual-of-used-update/{hiSystemsEmployeeId}")
    public CommonResult putAnnualOfUsed(@PathVariable long hiSystemsEmployeeId, @RequestBody AnnualOfUsedDataUpdateRequest updateRequest) {
        paidVacationService.putAnnualOfUsed(hiSystemsEmployeeId, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연차 사용 내역 전체조회")
    @GetMapping("/paid-vacation/all")
    public ListResult<PaidVacationItem> getPaidVacations() {
        return ResponseService.getListResult(paidVacationService.getPaidVacations(), true);
    }
}
