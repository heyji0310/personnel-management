package com.anhj.personnelmanagement.controller;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import com.anhj.personnelmanagement.model.CommonResult;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.SingleResult;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemEmployeeItem;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeUpdateRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeWithdrawalUpdateRequest;
import com.anhj.personnelmanagement.service.HiSystemEmployeeService;
import com.anhj.personnelmanagement.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee-management")
public class HiSystemEmployeeController {
    private final HiSystemEmployeeService hiSystemEmployeeService;

    @ApiOperation(value = "사원 등록")
    @PostMapping("/employee/new")
    public CommonResult setHiSystemEmployee(@RequestBody @Valid HiSystemsEmployeeRequest employeeRequest) {
        hiSystemEmployeeService.setHiSystemEmployee(employeeRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "loginId", value = "회원 로그인 아이디", required = true)
    )
    @GetMapping("/employee/{loginId}")
    public SingleResult<HiSystemEmployeeItem> getHiSystemEmployee(@PathVariable @Valid String loginId) {
        return ResponseService.getSingleResult(hiSystemEmployeeService.getHiSystemEmployee(loginId));
    }

    @ApiOperation(value = "부서별 조회")
    @GetMapping("/employee/search")
    public ListResult<HiSystemEmployeeItem> getHiSystemEmployees(
            @RequestParam(value = "departmentType", required = false) DepartmentType departmentType,
            @RequestParam(value = "positionType", required = false) PositionType positionType) {
        if (departmentType == null) {
            return ResponseService.getListResult(hiSystemEmployeeService.getHiSystemsEmployees(), true);
        } else if (positionType == null) {
            return ResponseService.getListResult(hiSystemEmployeeService.getHiSystemsEmployees(departmentType), true);
        } else
            return ResponseService.getListResult(hiSystemEmployeeService.getHiSystemsEmployees(positionType), true);
    }

    @ApiOperation(value = "사원 정보 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "loginId", value = "사원 로그인 아이디", required = true)
    )
    @PutMapping("/employee/{loginId}")
    public CommonResult putHiSystemEmployee(@PathVariable @ApiParam(example = "0") String loginId, @RequestBody(required = false) @Valid HiSystemsEmployeeUpdateRequest updateRequest) {
        hiSystemEmployeeService.putHiSystemEmployee(loginId, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "퇴사일자 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    )
    @PutMapping("/employee-withdrawal/{employeeId}")
    public CommonResult putDateWithdrawal(@PathVariable long employeeId, @RequestBody HiSystemsEmployeeWithdrawalUpdateRequest request) {
        hiSystemEmployeeService.putDateWithdrawal(employeeId, request);
        return ResponseService.getSuccessResult();
    }
}
