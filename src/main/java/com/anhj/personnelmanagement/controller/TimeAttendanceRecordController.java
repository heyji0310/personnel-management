package com.anhj.personnelmanagement.controller;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.model.CommonResult;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.SingleResult;
import com.anhj.personnelmanagement.model.timeAttendanceRecordModel.TimeAttendanceRecordItem;
import com.anhj.personnelmanagement.model.timeAttendanceRecordModel.TimeAttendanceRecordRequest;
import com.anhj.personnelmanagement.service.HiSystemEmployeeService;
import com.anhj.personnelmanagement.service.ResponseService;
import com.anhj.personnelmanagement.service.TimeAttendanceRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "출퇴근 기록부")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/time-attendance-record")
public class TimeAttendanceRecordController {
    private final HiSystemEmployeeService hiSystemEmployeeService;
    private final TimeAttendanceRecordService timeAttendanceRecordService;

    @ApiOperation(value = "출근 시간 기록")
    @PostMapping("/new/go-to-the-office")
    public CommonResult setTimeAttendanceRecord(@RequestBody TimeAttendanceRecordRequest request) {
        HiSystemsEmployee hiSystemsEmployee = hiSystemEmployeeService.getEmployeeData(request.getHiSystemsEmployeeId());
        timeAttendanceRecordService.setTimeAttendanceRecord(hiSystemsEmployee);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "퇴근 시간 기록")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "timeRecordId", value = "출퇴근기록부 시퀀스", required = true)
    )
    @PutMapping("/leave-the-office/{timeRecordId}")
    public CommonResult putLeaveTheOffice(@PathVariable long timeRecordId) {
        timeAttendanceRecordService.putLeaveTheOffice(timeRecordId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "외출 시간 기록")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "timeRecordId", value = "출퇴근기록부 시퀀스", required = true)
    )
    @PutMapping("/out-of-office/{timeRecordId}")
    public CommonResult putOutOfOffice(@PathVariable long timeRecordId) {
        timeAttendanceRecordService.putOutOfOffice(timeRecordId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "복귀 시간 기록")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "timeRecordId", value = "출퇴근기록부 시퀀스", required = true)
    )
    @PutMapping("/back-to-office/{timeRecordId}")
    public CommonResult putBackToOffice(@PathVariable long timeRecordId) {
        timeAttendanceRecordService.putBackToOffice(timeRecordId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "출퇴근기록부 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "timeRecordId", value = "출퇴근기록부 시퀀스", required = true)
    )
    @GetMapping("/search/{timeRecordId}")
    public SingleResult<TimeAttendanceRecordItem> getTimeAttendanceRecord(@PathVariable long timeRecordId) {
        return ResponseService.getSingleResult(timeAttendanceRecordService.getTimeAttendanceRecord(timeRecordId));
    }

    @ApiOperation(value = "출퇴근기록부 전체조회")
    @GetMapping("/all")
    public ListResult<TimeAttendanceRecordItem> getTimeAttendanceRecords() {
        return ResponseService.getListResult(timeAttendanceRecordService.getTimeAttendanceRecords(), true);
    }
}
