package com.anhj.personnelmanagement.service;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.enums.DepartmentType;
import com.anhj.personnelmanagement.enums.PositionType;
import com.anhj.personnelmanagement.exception.CMissingDataException;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemEmployeeItem;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeUpdateRequest;
import com.anhj.personnelmanagement.model.hiSystemsEployeeModel.HiSystemsEmployeeWithdrawalUpdateRequest;
import com.anhj.personnelmanagement.repository.HiSystemEmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HiSystemEmployeeService {
    private final HiSystemEmployeeRepository hiSystemEmployeeRepository;

    public HiSystemsEmployee getEmployeeData(long employeeId) {
        return hiSystemEmployeeRepository.findById(employeeId).orElseThrow(CMissingDataException::new);
    }

    public void setHiSystemEmployee(HiSystemsEmployeeRequest hiSystemsEmployeeRequest) {
        HiSystemsEmployee hiSystemsEmployee = new HiSystemsEmployee.HiSystemsEmployeeBuilder(hiSystemsEmployeeRequest).build();
        hiSystemEmployeeRepository.save(hiSystemsEmployee);
    }

    public HiSystemEmployeeItem getHiSystemEmployee(String loginId) {
        HiSystemsEmployee hiSystemsEmployee = hiSystemEmployeeRepository.findAllByLoginIdOrderByEmployeeNumberAsc(loginId);
        return new HiSystemEmployeeItem.HiSystemEmployeeItemBuilder(hiSystemsEmployee).build();
    }

    public ListResult<HiSystemEmployeeItem> getHiSystemsEmployees() {
        List<HiSystemsEmployee> hiSystemsEmployees = hiSystemEmployeeRepository.findAll();
        List<HiSystemEmployeeItem> result = new LinkedList<>();

        hiSystemsEmployees.forEach(hiSystemsEmployee -> {
            HiSystemEmployeeItem addItem = new HiSystemEmployeeItem.HiSystemEmployeeItemBuilder(hiSystemsEmployee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<HiSystemEmployeeItem> getHiSystemsEmployees(DepartmentType departmentType) {
        List<HiSystemsEmployee> hiSystemsEmployees = hiSystemEmployeeRepository.findAllByDepartmentTypeOrderByDepartmentTypeAsc(departmentType);
        List<HiSystemEmployeeItem> result = new LinkedList<>();

        hiSystemsEmployees.forEach(hiSystemsEmployee -> {
            HiSystemEmployeeItem addItem = new HiSystemEmployeeItem.HiSystemEmployeeItemBuilder(hiSystemsEmployee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<HiSystemEmployeeItem> getHiSystemsEmployees(PositionType positionType) {
        List<HiSystemsEmployee> hiSystemsEmployees = hiSystemEmployeeRepository.findAllByPositionTypeOrderByPositionTypeAsc(positionType);
        List<HiSystemEmployeeItem> result = new LinkedList<>();

        hiSystemsEmployees.forEach(hiSystemsEmployee -> {
            HiSystemEmployeeItem addItem = new HiSystemEmployeeItem.HiSystemEmployeeItemBuilder(hiSystemsEmployee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putHiSystemEmployee(String loginId, HiSystemsEmployeeUpdateRequest updateRequest) {
        HiSystemsEmployee hiSystemsEmployee = hiSystemEmployeeRepository.findAllByLoginIdOrderByEmployeeNumberAsc(loginId);
        hiSystemsEmployee.putHiSystemEmployeeLoginData(updateRequest);
        hiSystemEmployeeRepository.save(hiSystemsEmployee);
    }

    public void putDateWithdrawal(long employeeId, HiSystemsEmployeeWithdrawalUpdateRequest request) {
        HiSystemsEmployee hiSystemsEmployee = hiSystemEmployeeRepository.findById(employeeId).orElseThrow(CMissingDataException::new);
        hiSystemsEmployee.putDateWithdrawal(request);
        hiSystemEmployeeRepository.save(hiSystemsEmployee);
    }
}
