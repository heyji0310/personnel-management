package com.anhj.personnelmanagement.service;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.entity.PaidVacation;
import com.anhj.personnelmanagement.exception.CMissingDataException;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.paidVacationModel.AnnualOfUsedDataUpdateRequest;
import com.anhj.personnelmanagement.model.paidVacationModel.PaidVacationItem;
import com.anhj.personnelmanagement.model.paidVacationModel.PaidVacationRequest;
import com.anhj.personnelmanagement.repository.HiSystemEmployeeRepository;
import com.anhj.personnelmanagement.repository.PaidVacationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PaidVacationService {
    private final HiSystemEmployeeRepository hiSystemEmployeeRepository;
    private final PaidVacationRepository paidVacationRepository;

    public void setPaidVacation(HiSystemsEmployee hiSystemsEmployee, PaidVacationRequest request) {
        PaidVacation paidVacation = new PaidVacation.PaidVacationBuilder(hiSystemsEmployee, request).build();
        paidVacationRepository.save(paidVacation);
    }

    public void putAnnualOfUsed(long hiSystemsEmployeeId, AnnualOfUsedDataUpdateRequest updateRequest) {
        PaidVacation paidVacation = paidVacationRepository.findById(hiSystemsEmployeeId).orElseThrow(CMissingDataException::new);
        paidVacation.putAnnualOfUsedData(updateRequest);
        paidVacationRepository.save(paidVacation);
    }

    public ListResult<PaidVacationItem> getPaidVacations() {
        List<PaidVacation> paidVacations = paidVacationRepository.findAll();
        List<PaidVacationItem> result = new LinkedList<>();

        paidVacations.forEach(paidVacation -> {
            PaidVacationItem addItem = new PaidVacationItem.PaidVacationItemBuilder(paidVacation).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
