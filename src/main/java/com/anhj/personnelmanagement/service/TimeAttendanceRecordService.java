package com.anhj.personnelmanagement.service;

import com.anhj.personnelmanagement.entity.HiSystemsEmployee;
import com.anhj.personnelmanagement.entity.TimeAttendanceRecord;
import com.anhj.personnelmanagement.exception.CMissingDataException;
import com.anhj.personnelmanagement.model.ListResult;
import com.anhj.personnelmanagement.model.timeAttendanceRecordModel.TimeAttendanceRecordItem;
import com.anhj.personnelmanagement.repository.TimeAttendanceRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TimeAttendanceRecordService {
    private final TimeAttendanceRecordRepository timeAttendanceRecordRepository;

    public void setTimeAttendanceRecord(HiSystemsEmployee hiSystemsEmployee) {
        TimeAttendanceRecord timeAttendanceRecord = new TimeAttendanceRecord.TimeAttendanceRecordBuilder(hiSystemsEmployee).build();
        timeAttendanceRecordRepository.save(timeAttendanceRecord);
    }

    public void putLeaveTheOffice(long timeRecordId) {
        TimeAttendanceRecord timeAttendanceRecord = timeAttendanceRecordRepository.findById(timeRecordId).orElseThrow(CMissingDataException::new);
        timeAttendanceRecord.putLeaveTheOffice();
        timeAttendanceRecordRepository.save(timeAttendanceRecord);
    }

    public void putOutOfOffice(long timeRecordId) {
        TimeAttendanceRecord timeAttendanceRecord = timeAttendanceRecordRepository.findById(timeRecordId).orElseThrow(CMissingDataException::new);
        timeAttendanceRecord.putOutOfOffice();
        timeAttendanceRecordRepository.save(timeAttendanceRecord);
    }

    public void putBackToOffice(long timeRecordId) {
        TimeAttendanceRecord timeAttendanceRecord = timeAttendanceRecordRepository.findById(timeRecordId).orElseThrow(CMissingDataException::new);
        timeAttendanceRecord.putBackToOffice();
        timeAttendanceRecordRepository.save(timeAttendanceRecord);
    }

    public TimeAttendanceRecordItem getTimeAttendanceRecord(long timeAttendanceRecordId) {
        TimeAttendanceRecord timeAttendanceRecord = timeAttendanceRecordRepository.findById(timeAttendanceRecordId).orElseThrow(CMissingDataException::new);
        return new TimeAttendanceRecordItem.TimeAttendanceRecordItemBuilder(timeAttendanceRecord).build();
    }

    public ListResult<TimeAttendanceRecordItem> getTimeAttendanceRecords() {
        List<TimeAttendanceRecord> timeAttendanceRecords = timeAttendanceRecordRepository.findAll();
        List<TimeAttendanceRecordItem> result = new LinkedList<>();

        timeAttendanceRecords.forEach(timeAttendanceRecord -> {
            TimeAttendanceRecordItem addItem = new TimeAttendanceRecordItem.TimeAttendanceRecordItemBuilder(timeAttendanceRecord).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
